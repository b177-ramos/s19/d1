// JavaScript ES6 Updates





// **Exponent Operator -- added to simplify calculations for exponent of a given number

// instead of:

/*
const firstNum = Math.pow(8,2);
console.log(firstNum);
*/

// use ES6 update:
const secondNum= 8 ** 2;
console.log(secondNum);




console.log("----------------------------------------------------------------------------");



// **Template Literals -- writes strings without using the concatenation operator (+)
// variables are placed inside a placeholder (${})
// instead of:

let name = 'John';
/*
let message = "Hello " + name + "! Welcome to programming!";
console.log('Message without template literals:\n' + message);
*/

// use ES6 update:
let message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);
// Multi-Line
const anotherMessage = `
	${name} attended a math competition.
	He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.
`;
console.log(anotherMessage);

// another example
const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);




console.log("----------------------------------------------------------------------------");




// **[SECTION] Array Destructuring -- allows us to unpack elements within an array into distinct variables. allows naming array elements with variables instead of using index numbers

const fullName = ["Juan", "Tolits", "Dela Cruz"];
console.log(fullName[0]); //shows array index 0 "Juan"
console.log(fullName[1])
console.log(fullName[2])

// output: Hello Juan Tolits Dela Cruz! It's good to see you again.(default)
console.log("Hello " + fullName[0] + " " + fullName[1] + " " + fullName[2] + "! It's good to see you again.");

// Using array destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's good to see you again.`);




console.log("----------------------------------------------------------------------------");




// **[SECTION] Object Destructuring
const person = {
	givenName: "Jane",
	maidenName: "Miles",
	familyName: "Doe"
};

console.log(person.givenName, ); //(acess object givenName (Jane))
console.log(person.maidenName);
console.log(person.familyName);

// Hello Jane Miles Doe! It's good to see you again.
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Using object Destructuring -- SHOULD BE EXACT NAME OF THE OBJECT
const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName({givenName, maidenName, familyName}){
	console.log(`This is printed inside a function:`)
	console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person);


//turning to arrow function
//const getFullName = ({givenName, maidenName, familyName}) => {
// 	console.log(`This is printed inside a function:`)
// 	console.log(`${givenName} ${maidenName} ${familyName}`);
// }
// getFullName(person);




console.log("----------------------------------------------------------------------------");




// **[SECTION] Arrow Function
/* Syntax:
let/const functionName = () => {
	statement/s;
}
*/
const hello = () => {
	console.log(`Hello World!`);
}

hello();


// Arrow function with loops
const student = ["John", "Jojo", "Judy"];

//John is a student.
//Jojo is a student.
//Judy is a student.
student.forEach((student) => {
	console.log(`${student} is a student.`)
})




console.log("----------------------------------------------------------------------------");




// **[SECTION] Implicit Return Statement
const add = (x,y) => x + y;

let total = add(1,2);
console.log(total);




console.log("----------------------------------------------------------------------------");




// **[SECTION] Default function argument value

const greet = (name = 'User') => {
	return `Good evening, ${name}!`
};

console.log(greet()); //Good evening, User (set as default/no input)
console.log(greet('John')); ////Good evening, John




console.log("----------------------------------------------------------------------------");




// **[SECTION] Class-based object blueprint
// creating a class
class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object
const myCar = new Car();

myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;
myCar.color = "White";
console.log(myCar);

const myNewCar = new Car('Toyota', 'Vios', 2021);
console.log(myNewCar);